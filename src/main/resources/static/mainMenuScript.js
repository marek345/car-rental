$("#car-repository-li").click(function() {
	window.location.href='carRepositoryPage.html';
	return false;
});

$("#client-repository-li").click(function() {
	window.location.href='clientRepositoryPage.html';
	return false;
});

$("#car-rental-li").click(function() {
	window.location.href='rentalRepositoryPage.html';
	return false;
});

$("#car-rental-history-li").click(function() {
	window.location.href='rentalHistoryPage.html';
	return false;
});

$(document).ready(function(){ 
    $('body').fadeIn(500); 
});