function setSelectsData() {
	$.ajax({
		method: "GET",
		url: "http://localhost:8080/car",
		success: function(response) {
			console.log(response);
			fillCarSelect(response);			
		},
		timeout: 0
	});

	$.ajax({
		method: "GET",
		url: "http://localhost:8080/client",
		success: function(response) {
			console.log(response);
			fillClientSelect(response);
		},
		timeout: 0
	});
}

$("#search-button").click(function() {
	$("#rentals").fadeOut(150);
	sleep(150);
	
	$("#rentals").text("");
	
	var carId = getCarIdFromOption();	
	var clientId = getClientIdFromOption();		
	var startTime = $("#start-date-text-field").val();
	var endTime = $("#end-date-text-field").val();
	var url = "http://localhost:8080/carRental/searchRentalRepository?carId=" + carId 
		+ "&clientId=" + clientId + "&startTime=" + startTime + "&endTime=" + endTime;
	$.ajax({
		method: "GET",
		url: url,
		success: function(response) {
			$("#rentals").fadeIn(400); 
			console.log(response);
			fillRentalsUl(response);
		},
		timeout: 0
	});
	return false;	
});

function fillRentalsUl(rentals) {
	addHeadsToTable();
	for(var i = 0; i < rentals.length; i++) {
		addRentalToRentalsUl(rentals[i]);
	}
}

function addHeadsToTable() {
	var $head = $("<thead><tr><th>Klient, ID</th><th>Samochód, ID</th><th>"
	+ "Data wypożyczenia</th><th>Data oddania</th></tr></thead>");
	$carTable = $("#rentals");
	$carTable.append($head);
}

function addRentalToRentalsUl(rental) {
	var $rentalLi = $("<tr><td>" + rental.clientName + " , " + rental.clientId +
	"</td><td>" + rental.carDescription + " , " + rental.carId +
	"</td><td>" + rental.startDateTime + "</td><td>" + rental.endDateTime + "</td></tr>");
	$rentalsUl = $("#rentals");
	$rentalsUl.append($rentalLi);
	$rentalLi.click(function() {
		console.log(rental);
	});
}

function fillCarSelect(cars) {
	for(var i = 0; i < cars.length; i++) {
		addCarToCarsSelect(cars[i]);
	}
}
	
function addCarToCarsSelect(car) {
	var $carOption = $("<option>" + car.carBrand + " " + car.carModel + " " +
	car.carId + "</option>");
	$carSelect = $("#car-list");
	$carSelect.append($carOption);
	$carSelect.click(function() {
		console.log(car);
	});
}

function fillClientSelect(clients) {
	for(var i = 0; i < clients.length; i++) {
		addClientToClientsSelect(clients[i]);
	}
}
	
function addClientToClientsSelect(client) {
	var $clientOption = $("<option>" + client.surname + " " + client.name +
	" " + client.clientId + "</option>");
	$clientSelect = $("#client-list");
	$clientSelect.append($clientOption);
	$clientSelect.click(function() {
		console.log(client);
	});
}

function getCarIdFromOption() {
	var list = document.getElementById("car-list");
	var str = list.options[list.selectedIndex].value;
	var optionArray = str.split(" ");
	return optionArray[optionArray.length - 1];
}

function getClientIdFromOption() {
	var list = document.getElementById("client-list");
	var str = list.options[list.selectedIndex].value;
	var optionArray = str.split(" ");	
	return optionArray[optionArray.length - 1];	
}

$("#main-menu-button").click(function() {
	window.location.href='mainMenu.html';
	return false;
});

$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
	 window.location.href='mainMenu.html';
    }
});

$(document).ready(function(){ 
    $('body').fadeIn(500); 
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}