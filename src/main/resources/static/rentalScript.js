function setSelectsData() {
	$("#opt1").prop("checked", true);
	$("#opt1").change();
	$("#rent").text("");
	
	$.ajax({
		method: "GET",
		url: "http://localhost:8080/car/getAllUnrentedCars",
		success: function(response) {
			console.log(response);
			fillCarSelect(response);			
		},
		timeout: 0
	});
	
	$.ajax({
		method: "GET",
		url: "http://localhost:8080/client",
		success: function(response) {
			console.log(response);
			fillClientSelect(response);
		},
		timeout: 0
	});
}

function fillCarSelect(cars) {
	$carSelect = $("#car-list");
	$carSelect.find("option:not([disabled])").remove();
	for(var i = 0; i < cars.length; i++) {
		addCarToCarsSelect(cars[i]);
	}
}
	
function addCarToCarsSelect(car) {
	var $carOption = $("<option>" + car.carBrand + " " + car.carModel + " " +
	car.carId + "</option>");
	$carSelect = $("#car-list");
	$carSelect.append($carOption);
	$carSelect.click(function() {
		console.log(car);
	});
}

function fillClientSelect(clients) {
	$clientSelect = $("#client-list");
	$clientSelect.find("option:not([disabled])").remove();
	for(var i = 0; i < clients.length; i++) {
		addClientToClientsSelect(clients[i]);
	}
}
	
function addClientToClientsSelect(client) {
	var $clientOption = $("<option>" + client.surname + " " + client.name +
	" " + client.clientId + "</option>");
	$clientSelect = $("#client-list");
	$clientSelect.append($clientOption);
	$clientSelect.click(function() {
		console.log(client);
	});
}

$("#opt1").change(function() {
	document.getElementById("client-list").disabled = false;
	document.getElementById("name-text-field").disabled = true;
	document.getElementById("surname-text-field").disabled = true;
});

$("#opt2").change(function() {
	document.getElementById("client-list").disabled = true;
	document.getElementById("name-text-field").disabled = false;
	document.getElementById("surname-text-field").disabled = false;
});




$("#rent-button").click(function() {
	$("#correct-rental").fadeOut(150);
	$("#incorrect-rental").fadeOut(150);
	$("#incorrect-period").fadeOut(150);
	$("#incorrect-client").fadeOut(150);
	$("#rent").fadeOut(150);
	sleep(130);
	
	if(getCarIdFromOption() != 0 &&	$("#start-date-text-field").val() != 0 
	&& $("#end-date-text-field").val() != 0) {
		var clientId;
		if(document.getElementById("client-list").disabled == false
			&& getClientIdFromOption() != 0) { 
			clientId = getClientIdFromOption();
		} else {
			var name = $("#name-text-field").val();
			var surname = $("#surname-text-field").val();
			$.ajax({
				method: "POST",
				url: "http://localhost:8080/client",
				contentType:"application/json",
				data: JSON.stringify({
					clientName: name,
					clientSurname: surname
				}),
				success: function(response) {
					$("#name-text-field").val("");
					$("#surname-text-field").val("");
					console.log(response);
					clientId = response.clientId;
				},
				error: function () {
					$("#incorrect-client").fadeIn(400);
				},
				timeout: 0
			});
		}
		var carId = getCarIdFromOption();
		var startTime = $("#start-date-text-field").val();
		var endTime = $("#end-date-text-field").val();
	} else { $("#incorrect-rental").fadeIn(400); }
	
	var url = "http://localhost:8080/carRental/checkIfCarIsAvailable?startTime="
			+ startTime + "&endTime=" + endTime + "&carId=" + carId;	
			
	$.ajax({
		method: "GET",
		url: url,
		error: function () {
			$("#incorrect-rental").fadeIn(400);
		},		
		success: function(response) {
			if(response == true) {
				$.ajax({
					method: "POST",
					url: "http://localhost:8080/carRental",
					contentType:"application/json",
					data: JSON.stringify({
						carId: carId,
						clientId: clientId,
						startTime: startTime,
						endTime: endTime
					}),
		
					success: function(response) {			
						console.log(response);
						$("#correct-rental").fadeIn(400); 
						$("#rent").fadeIn(400); 
						setSelectsData();
						addRentalToRentalUl(response);
						$("#start-date-text-field").val("");
						$("#end-date-text-field").val("");			
					},
					error: function () {
						$("#incorrect-rental").fadeIn(400);
					},
					timeout: 0
				});
			} else  { $("#incorrect-period").fadeIn(400); } 			
		},
	});
	return false;
});

function getCarIdFromOption() {
	var list = document.getElementById("car-list");
	var str = list.options[list.selectedIndex].value;
	var optionArray = str.split(" ");
	return optionArray[optionArray.length - 1];
}

function getClientIdFromOption() {
	var list = document.getElementById("client-list");
	var str = list.options[list.selectedIndex].value;
	var optionArray = str.split(" ");	
	return optionArray[optionArray.length - 1];	
}

$("#main-menu-button").click(function() {
	window.location.href='mainMenu.html';
	return false;
});


function addRentalToRentalUl(rental) {
	var $head = $("<thead><tr><th>Klient, ID</th><th>Samochód, ID</th><th>"
	+ "Data wypożyczenia</th><th>Data oddania</th></tr></thead>");
	$carTable = $("#rent");
	$carTable.append($head);
	var $rentalLi = $("<tr><td>" + rental.clientName + " , " + rental.clientId +
	"</td><td>" + rental.carDescription + " , " + rental.carId +
	"</td><td>" + rental.startDateTime + "</td><td>" + rental.endDateTime + "</td></tr>");
	$rentalUl = $("#rent");
	$rentalUl.append($rentalLi);
	$rentalLi.click(function() {
		console.log(rental);
	});
}

$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
	 window.location.href='mainMenu.html';
    }
});

$(document).ready(function(){ 
    $('body').fadeIn(500); 
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

