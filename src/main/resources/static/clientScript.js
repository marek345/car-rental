function setClientUl() {
	hideAllTips();
	$("#clients").text("");
	$("#name-text-field").val("");
	$("#surname-text-field").val("");
	$("#id-text-field").val("");
	
	$.ajax({
		method: "GET",
		url: "http://localhost:8080/client",
		success: function(response) {
			console.log(response);
			fillClientUl(response);			
		},
		timeout: 0
	});
}

function addHeadsToTable() {
	var $head = $("<thead><tr><th>Nazwisko</th><th>Imię</th><th>ID</th></tr></thead>");
	$carTable = $("#clients");
	$carTable.append($head);
}

function fillClientUl(clients) {
	addHeadsToTable();
	for(var i = 0; i < clients.length; i++) {
		addClientToClientUl(clients[i]);
	}
}
	
function addClientToClientUl(client) {
	var $clientLi = $("<tr><td>" + client.surname + "</td><td>" + client.name +
	"</td><td>" + client.clientId + "</td></tr>");
	$clientUl = $("#clients");
	$clientUl.append($clientLi);
	$clientUl.click(function() {
		console.log(client);
	});
}

function addClientToDeletedClientUl(client) {
	var $clientLi = $("<li>" + client.surname + " " + client.name +
	", ID: " + client.clientId + "</li>");
	$clientUl = $("#deleted-client");
	$clientUl.append($clientLi);
	$clientUl.click(function() {
		console.log(client);
	});
}

$("#add-button").click(function() {
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	$("#clients").fadeOut(150);
	var name = $("#name-text-field").val();
	var surname = $("#surname-text-field").val();
	sleep(130);
	$.ajax({
		method: "POST",
		url: "http://localhost:8080/client",
		contentType:"application/json",
		data: JSON.stringify({
			clientName: name,
			clientSurname: surname
		}),
		success: function(response) {
			$("#name-text-field").val("");
			$("#surname-text-field").val("");
			console.log(response);
			$("#clients").text("");
			addHeadsToTable();
			addClientToClientUl(response);
			$("#correctSave").fadeIn(400);
			$("#clients").fadeIn(400);	
		},
		error: function() {			
			$("#incorrectSave").fadeIn(400);
		},
		timeout: 0
	});
	return false;
});

function hideAllTips() {	
	$("#deleted-client").fadeOut(150);
	$("#incorrectDelete").fadeOut(150);
	$("#correctDelete").fadeOut(150);
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	sleep(150);
	$("#deleted-client").text("");
}

$("#remove-button").click(function() {
	$("#clients").fadeOut(150);
	hideAllTips();
	var id = $("#id-text-field").val();
	$.ajax({
		method: "POST",
		url: "http://localhost:8080/client/deleteClient?id=" + id,
		success: function(response) {
			console.log(response);
			setClientUl();
			if(response.length != 0) { 
				addClientToDeletedClientUl(response);
				$("#clients").fadeIn(400);
				$("#correctDelete").fadeIn(400);
				$("#deleted-client").fadeIn(400);
			}
			else {
				$("#incorrectDelete").fadeIn(400);
				$("#clients").fadeIn(400);
			}			
		},
		error: function() {
			$("#incorrectDelete").fadeIn(400);
			$("#clients").fadeIn(400);
		},
		timeout: 0
	});
	return false;
});

$("#search-button").click(function() {
	$("#clients").fadeOut(150);
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	sleep(150);
	$("#clients").text("");
	var name = $("#name-search-field").val();
	var surname = $("#surname-search-field").val();
	var url = "http://localhost:8080/client/searchClientRepository?name=" 
		+ name + "&surname=" + surname;	
	
	$.ajax({
		method: "GET",
		url: url,
		success: function(response) {
			fillClientUl(response);
			$("#clients").fadeIn(400); 
			console.log(response);			
		},
		timeout: 0
	});
	return false
});

$("#main-menu-button").click(function() {
	window.location.href='mainMenu.html';
	return false;
});

$("#main-menu-button2").click(function() {
	window.location.href='mainMenu.html';
	return false;
});

$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
	 window.location.href='mainMenu.html';
    }
});

$(document).ready(function(){ 
    $('body').fadeIn(500); 
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

