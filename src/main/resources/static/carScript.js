function setCarUl() {
	hideAllTips();	
	$("#cars").text("");
	$("#brand-text-field").val("");
	$("#model-text-field").val("");
	$("#id-text-field").val("");	
	var url = "http://localhost:8080/car";
	
	$.ajax({
		method: "GET",
		url: url,
		success: function(response) {
			$("#cars").fadeIn(400);
			console.log(response);
			fillCarUl(response);
		},
		timeout: 0
	});
}

function fillCarUl(cars) {
	$carUl = $("#cars");
	addHeadsToTable();
	$("#cars").append("<tbody>")
	for(var i = 0; i < cars.length; i++) {
		addCarToCarUl(cars[i]);
	}
	$("#cars").append("</tbody>")
}
function addHeadsToTable() {
	var $head = $("<thead><tr><th>Marka</th><th>Model</th><th>ID</th></tr></thead>");
	$carTable = $("#cars");
	$carTable.append($head);
}

function addCarToCarUl(car) {
	var $carLi = $("<tr><td>" + car.carBrand + "</td><td>" + car.carModel +
	"</td><td>" + car.carId + "</td></tr>");
	$carUl = $("#cars");
	$carUl.append($carLi);
	$carUl.click(function() {
		console.log(car);
	});
}

$("#add-button").click(function() {	
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	$("#cars").fadeOut(150);
	var brand = $("#brand-text-field").val();
	var model = $("#model-text-field").val();
	sleep(130);
	$.ajax({
		method: "POST",
		url: "http://localhost:8080/car",
		contentType:"application/json",
		data: JSON.stringify({
			brand: brand,
			model: model
		}),
		success: function(response) {
			$("#brand-text-field").val("");
			$("#model-text-field").val("");
			console.log(response);
			$("#cars").text("");
			addHeadsToTable();
			addCarToCarUl(response);
			$("#correctSave").fadeIn(400);
			$("#cars").fadeIn(400);			
		},
		error: function() {			
			$("#incorrectSave").fadeIn(400);
		},
		timeout: 0
	});
	return false;
});

$("#remove-button").click(function() {
	$("#cars").fadeOut(150);	
	hideAllTips();	
	var id = $("#id-text-field").val();
	$.ajax({
		method: "POST",
		url: "http://localhost:8080/car/deleteCar?carId=" + id,
		success: function(response) {
			console.log(response);
			setCarUl();
			if(response.length != 0) { 
				addCarToDeletedCarUl(response);
				$("#cars").fadeIn(400);	
				$("#correctDelete").fadeIn(400);
				$("#deleted-car").fadeIn(400);
			}
			else {
				$("#incorrectDelete").fadeIn(400);
				$("#cars").fadeIn(400);	
			}			
		},
		error: function() {
			$("#incorrectDelete").fadeIn(400);
			$("#cars").fadeIn(400);	
		},
		timeout: 0
	});
	return false;
});

function hideAllTips() {
	$("#deleted-car").fadeOut(150);
	$("#incorrectDelete").fadeOut(150);
	$("#correctDelete").fadeOut(150);
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	sleep(150);
	$("#deleted-car").text("");
}

function addCarToDeletedCarUl(car) {
	var $carLi = $("<li>" + car.carBrand + " " + car.carModel + " " 
	+ ", ID: " + car.carId + "</li>");
	$carUl = $("#deleted-car");
	$carUl.append($carLi);
	$carUl.click(function() {
		console.log(car);
	});
}

$("#search-button").click(function() {
	$("#cars").fadeOut(150);
	$("#correctSave").fadeOut(150);
	$("#incorrectSave").fadeOut(150);
	sleep(150);
	$("#cars").text("");
	
	var brand = $("#brand-search-field").val();
	var model = $("#model-search-field").val();
	var url = "http://localhost:8080/car/searchCarRepository?brand=" 
		+ brand + "&model=" + model;
	
	$.ajax({
		method: "GET",
		url: url,
		success: function(response) {
			fillCarUl(response);
			$("#cars").fadeIn(400); 
			console.log(response);			
		},
		timeout: 0
	});
	return false
});

$("#main-menu-button").click(function() {
	window.location.href='mainMenu.html';
	return false;
});

$("#main-menu-button2").click(function() {
	window.location.href='mainMenu.html';
	return false;
});

$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
	 window.location.href='mainMenu.html';
    }
});

$(document).ready(function(){ 
    $('body').fadeIn(500); 
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}