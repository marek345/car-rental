package pl.marektrejtowicz.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.format.annotation.DateTimeFormat;
import pl.marektrejtowicz.configuration.jsonserialization.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class CarRentalRequestModel {
    private Integer carId;
    private Integer clientId;
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startTime;
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endTime;

    public CarRentalRequestModel() {

    }

    public CarRentalRequestModel(Integer carId, Integer clientId, LocalDateTime startTime, LocalDateTime endTime) {
        this.carId = carId;
        this.clientId = clientId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
