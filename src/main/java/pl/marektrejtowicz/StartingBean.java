package pl.marektrejtowicz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.marektrejtowicz.entity.Car;
import pl.marektrejtowicz.entity.Client;
import pl.marektrejtowicz.repository.CarRentalRepository;
import pl.marektrejtowicz.repository.CarRepository;
import pl.marektrejtowicz.repository.ClientRepository;

import javax.annotation.PostConstruct;
import javax.swing.*;

@Component
public class StartingBean {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CarRentalRepository carRentalRepository;

    @PostConstruct
    private void onInit(){
    }
}