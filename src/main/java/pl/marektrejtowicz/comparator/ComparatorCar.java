package pl.marektrejtowicz.comparator;

import pl.marektrejtowicz.entity.Car;

import java.util.Comparator;

public class ComparatorCar implements Comparator<Car>{

    @Override
    public int compare(Car firstCar, Car secondCar) {
        return firstCar.compareTo(secondCar);
    }
}
