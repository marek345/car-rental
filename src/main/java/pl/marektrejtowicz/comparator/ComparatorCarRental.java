package pl.marektrejtowicz.comparator;

import pl.marektrejtowicz.entity.CarRental;

import java.util.Comparator;

public class ComparatorCarRental implements Comparator<CarRental> {
    @Override
    public int compare(CarRental carRental1, CarRental carRental2) {
        return carRental1.compareTo(carRental2);
    }
}
