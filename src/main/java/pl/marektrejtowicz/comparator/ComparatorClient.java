package pl.marektrejtowicz.comparator;

import pl.marektrejtowicz.entity.Client;

import java.util.Comparator;

public class ComparatorClient implements Comparator<Client> {
    @Override
    public int compare(Client firstClient, Client secondClient) {
        return firstClient.compareTo(secondClient);
    }
}
