package pl.marektrejtowicz.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.marektrejtowicz.entity.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Integer> {
}
