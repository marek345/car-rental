package pl.marektrejtowicz.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.marektrejtowicz.entity.DeletedCar;

@Repository
public interface DeletedCarRepository extends CrudRepository<DeletedCar, Integer> {
}
