package pl.marektrejtowicz.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.marektrejtowicz.entity.DeletedClient;

@Repository
public interface DeletedClientRepository extends CrudRepository<DeletedClient, Integer> {
}
