package pl.marektrejtowicz.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.marektrejtowicz.entity.CarRental;

@Repository
public interface CarRentalRepository extends CrudRepository<CarRental, Integer> {
}
