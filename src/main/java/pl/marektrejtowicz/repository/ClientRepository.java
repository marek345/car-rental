package pl.marektrejtowicz.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.marektrejtowicz.entity.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {
}
