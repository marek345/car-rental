package pl.marektrejtowicz.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Client implements Comparable<Client>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Length(min = 3, max = 30)
    private String clientName;
    @Length(min = 3, max = 30)
    private String clientSurname;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "client")
    private List<CarRental> car = new ArrayList<>();

    public Client() {}

    public Client( String clientName, String clientSurname) {
        this.clientName = clientName;
        this.clientSurname = clientSurname;
    }

    public Client(String clientName, String clientSurname, Integer id) {
        this.clientName = clientName;
        this.clientSurname = clientSurname;
        this.id = id;
    }

    @JsonProperty
    public Integer getClientId() { return getId(); }

    @JsonProperty
    public String getName() { return getClientName(); }

    @JsonProperty
    public String getSurname() { return getClientSurname(); }

    public List<CarRental> getCar() {
        return car;
    }

    public void setCar(List<CarRental> car) {
        this.car = car;
    }

    public String getClientSurname() {
        return clientSurname;
    }

    public void setClientSurname(String clientSurname) {
        this.clientSurname = clientSurname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Override
    public String toString() {
        return clientSurname + ", " + clientName + ", " + id;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (clientName != null ? clientName.hashCode() : 0);
        result = 31 * result + (clientSurname != null ? clientSurname.hashCode() : 0);
        result = 31 * result + (car != null ? car.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Client client) {
        int surnameCompare = clientSurname.compareTo(client.getClientSurname());

        if(surnameCompare == 0) {
            return clientName.compareTo(client.getClientName());
        } else {
            return surnameCompare;
        }
    }
}
