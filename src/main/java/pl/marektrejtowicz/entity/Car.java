package pl.marektrejtowicz.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Car implements Comparable<Car>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Length(min = 2, max = 20)
    private String model;
    @Length(min = 2, max = 20)
    private String brand;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "car")
    private List<CarRental> rentals = new ArrayList<>();

    public Car() {}

    public Car(String model, String brand) {
        this.model = model;
        this.brand = brand;
    }

    public Car(String model, String brand, Integer id) {
        this.model = model;
        this.brand = brand;
        this.id = id;

    }

    public Car(Car car) {
        this.model = car.getModel();
        this.brand = car.getBrand();
        this.id = car.getId();

    }

    @JsonProperty
    public Integer getCarId() { return getId();}

    @JsonProperty
    public String getCarBrand() { return getBrand();}

    @JsonProperty
    public String getCarModel() { return getModel();}


    public void setId(Integer id) {
        this.id = id;
    }

    public List<CarRental> getRentals() {
        return rentals;
    }

    public void setRentals(List<CarRental> rentals) {
        this.rentals = rentals;
    }

    public Integer getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getBrand() {
        return brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }



    @Override
    public String toString() {
        return brand + ", " + model + ", " + id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (id != null ? !id.equals(car.id) : car.id != null) return false;
        if (model != null ? !model.equals(car.model) : car.model != null) return false;
        if (brand != null ? !brand.equals(car.brand) : car.brand != null) return false;
        return rentals != null ? rentals.equals(car.rentals) : car.rentals == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (brand != null ? brand.hashCode() : 0);
        result = 31 * result + (rentals != null ? rentals.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Car car) {
        int brandCompare = brand.compareTo(car.getBrand());

        if(brandCompare == 0) {
           return model.compareTo(car.getModel());
        } else {
            return brandCompare;
        }
    }
}
