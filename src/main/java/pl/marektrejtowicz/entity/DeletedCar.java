package pl.marektrejtowicz.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DeletedCar {
    @Id
    private Integer id;

    public DeletedCar(Integer id) { this.id = id; }

    public DeletedCar() {}

    public Integer getId() { return id; }

    public void setId(Integer id) {
        this.id = id;
    }
}
