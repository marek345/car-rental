package pl.marektrejtowicz.entity;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DeletedClient {
    @Id
    private Integer id;

    public DeletedClient(Integer id) {
        this.id = id;
    }

    public DeletedClient() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
