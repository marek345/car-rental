package pl.marektrejtowicz.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;
import pl.marektrejtowicz.configuration.jsonserialization.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class CarRental implements Comparable<CarRental> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    private Client client;
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    private Car car;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm")
    private LocalDateTime startDateTime;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @DateTimeFormat(pattern =  "yyyy-MM-dd HH:mm")
    private LocalDateTime endDateTime;

    public CarRental() {}

    public CarRental(Client client, Car car, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.client = client;
        this.car = car;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    @JsonProperty
    public Integer getClientId() {
        return client.getId();
    }

    @JsonProperty
    public Integer getCarId() {
        return car.getId();
    }

    @JsonProperty
    public String getClientName() {
        return client.getClientName() + " " + client.getClientSurname();
    }

    @JsonProperty
    public String  getCarDescription() {
        return car.getBrand() + " " + car.getModel();
    }

    @JsonProperty
    public Integer getCarRentalId() { return getId(); }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    @Override
    public String toString() {
        return  "Data wypożyczenia: " + startDateTime + ", Data oddania: " + endDateTime + ", Klient: " + client
                + ", Samochód: " + car + ", ID wypożyczenia: " + id;
    }

    @Override
    public int compareTo(CarRental carRental) {
        int startDateCompare = carRental.getStartDateTime().compareTo(startDateTime);

        if(startDateCompare == 0) {
            return carRental.getEndDateTime().compareTo(endDateTime);
        }
        return startDateCompare;
    }
}
