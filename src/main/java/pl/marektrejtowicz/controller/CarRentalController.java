package pl.marektrejtowicz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marektrejtowicz.comparator.ComparatorCarRental;
import pl.marektrejtowicz.entity.Car;
import pl.marektrejtowicz.entity.CarRental;
import pl.marektrejtowicz.entity.Client;
import pl.marektrejtowicz.repository.CarRentalRepository;
import pl.marektrejtowicz.repository.CarRepository;
import pl.marektrejtowicz.repository.ClientRepository;
import pl.marektrejtowicz.request.CarRentalRequestModel;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;

@RestController
@RequestMapping("/carRental")
public class CarRentalController {
    @Autowired
    private CarRentalRepository carRentalRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CarRepository carRepository;
    ComparatorCarRental comparatorCarRental = new ComparatorCarRental();

    @RequestMapping(method = RequestMethod.POST)
    public CarRental createCarRental(@RequestBody @Valid CarRentalRequestModel requestModel) {
        Client client = clientRepository.findOne(requestModel.getClientId());
        Car car = carRepository.findOne(requestModel.getCarId());
        LocalDateTime startTime = requestModel.getStartTime();
        LocalDateTime endTime = requestModel.getEndTime();

        if(client != null && car != null && startTime != null && endTime != null) {
            CarRental carRental = new CarRental(client, car, startTime, endTime);
            return carRentalRepository.save(carRental);
        }
        return null;
    }

    @RequestMapping(path = "/checkIfCarIsAvailable")
    public boolean checkIfCarIsAvailable(@RequestParam("startTime") LocalDateTime startTime,
                                         @RequestParam("endTime") LocalDateTime endTime,
                                         @RequestParam("carId") Integer carId) {
        for(CarRental carRental : carRepository.findOne(carId).getRentals()) {
            if(carRental.getCar().getId() == carId) {
            if((endTime.isBefore(carRental.getEndDateTime()) && endTime.isAfter(carRental.getStartDateTime())) ||
                    (endTime.isAfter(carRental.getEndDateTime()) && startTime.isBefore(carRental.getStartDateTime())) ||
                    (startTime.isBefore(carRental.getEndDateTime()) && endTime.isAfter(carRental.getEndDateTime())))
            { return false; } }
        }
        return true;
    }

    @RequestMapping(path = "/searchRentalRepository")
    public Iterable<CarRental> searchRepository(
            @RequestParam("carId") Integer carId,
            @RequestParam("clientId") Integer clientId,
            @RequestParam("startTime") LocalDateTime startTime,
            @RequestParam("endTime") LocalDateTime endTime) {
        ArrayList<CarRental> rentalsToLoop = new ArrayList<>();
        ArrayList<CarRental> foundRentals = new ArrayList<>();
        boolean ifChangeRentalsToLoop = false;

        overwriteListByAllRentals(rentalsToLoop);
        overwriteListByAllRentals(foundRentals);

        if (carId != 0) {
            foundRentals.clear();
            for (CarRental carRental : rentalsToLoop) {
                if (carId == carRental.getCarId()) {
                    foundRentals.add(carRental);
                    ifChangeRentalsToLoop = true;
                }
            }
        }

        overwriteRentalsToLoop(rentalsToLoop, foundRentals, ifChangeRentalsToLoop);

        if (clientId != 0) {
            foundRentals.clear();
            for (CarRental carRental : rentalsToLoop) {
                if (clientId == carRental.getClientId()) {
                    foundRentals.add(carRental);
                    ifChangeRentalsToLoop = true;
                }
            }
        }

        overwriteRentalsToLoop(rentalsToLoop, foundRentals, ifChangeRentalsToLoop);

        if (startTime != null) {
            foundRentals.clear();
            for (CarRental carRental : rentalsToLoop) {
                if (startTime.isBefore(carRental.getStartDateTime())) {
                    foundRentals.add(carRental);
                    ifChangeRentalsToLoop = true;
                }
            }
        }

        overwriteRentalsToLoop(rentalsToLoop, foundRentals, ifChangeRentalsToLoop);

        if (endTime != null) {
            foundRentals.clear();
            for (CarRental carRental : rentalsToLoop) {
                if (endTime.isAfter(carRental.getEndDateTime())) {
                    foundRentals.add(carRental);
                }
            }
        }

        foundRentals.sort(comparatorCarRental);
        return foundRentals;
    }

    public void overwriteListByAllRentals(ArrayList<CarRental> listToOverwrite) {
        Iterable<CarRental> allRentals = carRentalRepository.findAll();
        for (CarRental carRental : allRentals) {
            listToOverwrite.add(carRental);
        }
    }

    public void overwriteRentalsToLoop(ArrayList<CarRental> listToOverwrite, ArrayList<CarRental> originList, boolean check) {
        if(check){
            listToOverwrite.clear();
            for (CarRental carRental : originList) { listToOverwrite.add(carRental); }
        }
    }

    @RequestMapping
    public Iterable<CarRental> getAllRentals() {
        return carRentalRepository.findAll();
    }
}
