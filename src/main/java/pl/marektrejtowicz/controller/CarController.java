package pl.marektrejtowicz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marektrejtowicz.comparator.ComparatorCar;
import pl.marektrejtowicz.entity.Car;
import pl.marektrejtowicz.entity.CarRental;
import pl.marektrejtowicz.entity.DeletedCar;
import pl.marektrejtowicz.repository.CarRentalRepository;
import pl.marektrejtowicz.repository.CarRepository;
import pl.marektrejtowicz.repository.DeletedCarRepository;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private DeletedCarRepository deletedCarRepository;
    @Autowired
    private CarRentalRepository carRentalRepository;
    private ComparatorCar carComparator = new ComparatorCar();

    @RequestMapping(method = RequestMethod.POST)
    public Car createCar(@RequestBody @Valid Car car) {
        return carRepository.save(car);
    }

    @RequestMapping(path = "/deleteCar", method = RequestMethod.POST)
    public Car deleteCar(@RequestParam("carId") Integer id) {
        if (carRepository.exists(id)) {
            deletedCarRepository.save(new DeletedCar(id));
            return carRepository.findOne(id);
        }
        return null;
    }

    @RequestMapping
    public ArrayList<Car> getAllCars() {
        Iterable<Car> allCars = carRepository.findAll();
        ArrayList carList = new ArrayList();

        for(Car car : allCars) {
            if (!deletedCarRepository.exists(car.getId())) {
                carList.add(car);
            }
        }
        carList.sort(carComparator);
        return carList;
    }

    @RequestMapping(path = "/getAllUnrentedCars")
    public ArrayList<Car> getAllUnrentedCars() {
        Iterable<Car> allCars = getAllCars();
        ArrayList<Car> allUnrentedCars = new ArrayList<>();
        boolean save;

        for(Car car : allCars) {
            save = true;
            for (CarRental carRental : car.getRentals()) {
                if (carRental.getEndDateTime().isAfter(currentDateTime())&&
                        !carRental.getStartDateTime().isAfter(currentDateTime())) {
                    save = false;
                }
            }
            if(save) { allUnrentedCars.add(car); }
        }
        return allUnrentedCars;
    }

    public LocalDateTime currentDateTime() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        return currentDateTime;
    }

    @RequestMapping(path = "/searchCarRepository")
    public Iterable<Car> searchRepository(
            @RequestParam("brand") String brand,
            @RequestParam("model") String model) {
        ArrayList<Car> carsToLoop = new ArrayList<>();
        ArrayList<Car> foundCars = new ArrayList<>();
        boolean ifChangeCarsToLoop = false;

        overwriteListByAllCars(carsToLoop);
        overwriteListByAllCars(foundCars);

        if (!brand.equals("")) {
            foundCars.clear();
            for (Car car : carsToLoop) {
                if (checkIfWrordContainsChars(brand, car.getBrand())) {
                    foundCars.add(car);
                    ifChangeCarsToLoop = true;
                }
            }
        }

        overwriteCarsToLoop(carsToLoop, foundCars, ifChangeCarsToLoop);

        if (!model.equals("")) {
            foundCars.clear();
            for (Car car : carsToLoop) {
                if (checkIfWrordContainsChars(model, car.getModel())) {
                    foundCars.add(car);
                }
            }
        }

        return foundCars;
    }

    private boolean checkIfWrordContainsChars(String wordToCheck, String originalWord) {
        char[] charactersToCheck = wordToCheck.toLowerCase().toCharArray();
        char[] originalCharacters = originalWord.toLowerCase().toCharArray();

        for(int i = 0; i < charactersToCheck.length; i++) {
            if(charactersToCheck[i] != originalCharacters[i]) { return false; }
        }
        return true;
    }

    private void overwriteListByAllCars(ArrayList<Car> listToOverwrite) {
        Iterable<Car> allCars = getAllCars();
        for (Car car : allCars) {
            listToOverwrite.add(car);
        }
    }

    private void overwriteCarsToLoop(ArrayList<Car> listToOverwrite, ArrayList<Car> originList, boolean check) {
        if(check){
            listToOverwrite.clear();
            for (Car car : originList) { listToOverwrite.add(car); }
        }
    }
}
