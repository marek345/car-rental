package pl.marektrejtowicz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.marektrejtowicz.comparator.ComparatorClient;
import pl.marektrejtowicz.entity.Client;
import pl.marektrejtowicz.entity.DeletedClient;
import pl.marektrejtowicz.repository.CarRentalRepository;
import pl.marektrejtowicz.repository.ClientRepository;
import pl.marektrejtowicz.repository.DeletedClientRepository;

import javax.validation.Valid;
import java.util.ArrayList;

@RequestMapping("/client")
@RestController
public class ClientController {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private DeletedClientRepository deletedClientRepository;
    @Autowired
    private CarRentalRepository carRentalRepository;
    private ComparatorClient clientComparator =  new ComparatorClient();

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody @Valid Client client) {
        return clientRepository.save(client);
    }

    @RequestMapping(path = "/deleteClient", method = RequestMethod.POST)
    public Client deleteCient(@RequestParam("id") Integer id) {
        if(clientRepository.exists(id)) {
            deletedClientRepository.save(new DeletedClient(id));
            return clientRepository.findOne(id);
        }
        return null;
    }

    @RequestMapping
    public ArrayList<Client> getAllClients() {
        Iterable<Client> allClients =  clientRepository.findAll();
        ArrayList<Client> clientList = new ArrayList<>();

        for(Client client : allClients) {
            if(!deletedClientRepository.exists(client.getId())) { clientList.add(client); }
        }
        clientList.sort(clientComparator);
        return clientList;
    }

    @RequestMapping(path = "/searchClientRepository")
    public Iterable<Client> searchRepository(
            @RequestParam("name") String name,
            @RequestParam("surname") String surname) {
        ArrayList<Client> clientsToLoop = new ArrayList<>();
        ArrayList<Client> foundClients = new ArrayList<>();
        boolean ifChangeCarsToLoop = false;

        overwriteListByAllClients(clientsToLoop);
        overwriteListByAllClients(foundClients);

        if (!name.equals("")) {
            foundClients.clear();
            for (Client client : clientsToLoop) {
                if (checkIfWrordContainsChars(name, client.getName())) {
                    foundClients.add(client);
                    ifChangeCarsToLoop = true;
                }
            }
        }

        overwriteClientsToLoop(clientsToLoop, foundClients, ifChangeCarsToLoop);

        if (!surname.equals("")) {
            foundClients.clear();
            for (Client client : clientsToLoop) {
                if (checkIfWrordContainsChars(surname, client.getSurname())) {
                    foundClients.add(client);
                }
            }
        }

        return foundClients;
    }

    private boolean checkIfWrordContainsChars(String wordToCheck, String originalWord) {
        char[] charactersToCheck = wordToCheck.toLowerCase().toCharArray();
        char[] originalCharacters = originalWord.toLowerCase().toCharArray();

        for(int i = 0; i < charactersToCheck.length; i++) {
            if(charactersToCheck[i] != originalCharacters[i]) { return false; }
        }
        return true;
    }

    private void overwriteListByAllClients(ArrayList<Client> listToOverwrite) {
        Iterable<Client> allClients = getAllClients();
        for (Client client : allClients) {
            listToOverwrite.add(client);
        }
    }

    private void overwriteClientsToLoop(ArrayList<Client> listToOverwrite, ArrayList<Client> originList, boolean check) {
        if(check){
            listToOverwrite.clear();
            for (Client client : originList) { listToOverwrite.add(client); }
        }
    }
}
