package pl.marektrejtowicz.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.marektrejtowicz.configuration.StringToLocalDateTimeConverter;

import java.time.LocalDateTime;

@Configuration
public class ConverterConfiguration {
    @Bean
    public StringToLocalDateTimeConverter getStringToLocalDateTimeConverter() {
        System.out.println(123);
        return new StringToLocalDateTimeConverter();
    }
}
