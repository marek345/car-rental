package pl.marektrejtowicz.configuration.jsonserialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import pl.marektrejtowicz.configuration.StringToLocalDateTimeConverter;

import java.time.LocalDateTime;

@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean
    public StringToLocalDateTimeConverter getStringToLocalDateTimeConverter() {
        return new StringToLocalDateTimeConverter();
    }
}
